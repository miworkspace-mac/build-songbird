#!/bin/bash


NEWLOC=`curl -L "https://sourceforge.net/projects/songbird.mirror/files/latest/download" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep '.dmg' | head -1 | awk -F'url=' '{print $2}' | sed 's/\>//' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
